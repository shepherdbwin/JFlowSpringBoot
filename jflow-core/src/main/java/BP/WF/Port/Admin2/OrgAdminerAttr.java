package BP.WF.Port.Admin2;

import BP.DA.*;
import BP.Web.*;
import BP.En.*;
import BP.Port.*;
import BP.WF.*;
import BP.WF.Port.*;
import java.util.*;

/** 
 组织管理员
*/
public class OrgAdminerAttr
{
	/** 
	 管理员
	*/
	public static final String FK_Emp = "FK_Emp";
	/** 
	 组织
	*/
	public static final String OrgNo = "OrgNo";
}
package BP.WF.Template;

import BP.DA.*;
import BP.En.*;
import BP.WF.Port.*;
import BP.WF.*;
import java.util.*;

/** 
 节点用户组属性	  
*/
public class NodeTeamAttr
{
	/** 
	 节点
	*/
	public static final String FK_Node = "FK_Node";
	/** 
	 用户组
	*/
	public static final String FK_Team = "FK_Team";
}
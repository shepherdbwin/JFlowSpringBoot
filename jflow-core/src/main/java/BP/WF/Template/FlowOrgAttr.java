package BP.WF.Template;

import BP.DA.*;
import BP.En.*;
import BP.WF.Port.*;
import BP.WF.*;
import java.util.*;

/** 
 流程对应组织属性
*/
public class FlowOrgAttr
{
	/** 
	 流程
	*/
	public static final String FlowNo = "FlowNo";
	/** 
	 组织
	*/
	public static final String OrgNo = "OrgNo";
}